$(".toggle").click(function() {
    var accordion = $(this).parent().parent();
    if (!accordion.hasClass("active")) {
        $(".accordion").removeClass("active");
        accordion.addClass("active");
    } else if (accordion.hasClass("active")) {
        accordion.removeClass("active");
    }
})

$(".up-button").click(function() {
    var content = $(this).parent().parent();
    content.prev().insertAfter(content);
})

$(".down-button").click(function() {
    var content = $(this).parent().parent();
    content.next().insertBefore(content);
})