from django.test import TestCase,Client
from . import views
from django.urls import resolve
from django.contrib.auth.models import User

class UserBaseTest(TestCase):

    def test_signup_url(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code,200)

    def test_login_url(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)

    def test_signup_template(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response,'signup.html')

    def test_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response,'registration/login.html')

    def test_signup_page(self):
        response = Client().get('/signup/')
        fields = response.content.decode('utf8')
        self.assertIn("SIGN UP", fields)
        self.assertIn("First name", fields)
        self.assertIn("Last name", fields)
        self.assertIn("Email", fields)
        self.assertIn("Username", fields)
        self.assertIn("Password", fields)
        self.assertIn("Password confirmation", fields)

    def test_login_page(self):
        response = Client().get('/login/')
        fields = response.content.decode('utf8')
        self.assertIn("LOG IN", fields)
        self.assertIn("Username", fields)
        self.assertIn("Password", fields)

    def test_func_signup(self):
        find = resolve('/signup/')
        self.assertEqual(find.func, views.signup)

    def test_create_account(self):
        response = Client().post('/signup/', {'first_name': 'John', 'last_name': 'Doe', 'email' : 'johndoe@gmail.com', 'username':'johndoe', 'password1' : 'ashlabogan123', 'password2' : 'ashlabogan123'}, format='text/html')
        count = User.objects.count()
        self.assertEqual(count, 1)
