from django.urls import path
from . import views

app_name = 'userbase'

urlpatterns = [
    path('signup/', views.signup, name='signup'),
]