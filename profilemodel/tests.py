from django.test import TestCase
from django.test import Client
from .models import Course, Activity, Participant
from . import models

class ActivityTest(TestCase):
    
    # Tests for Story 3

    def test_url_home(self):
        response = Client().get('/')
        self.assertEqual(200, response.status_code)

    def test_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_url_story1(self):
        response = Client().get('/story1/')
        self.assertEqual(200, response.status_code)

    def test_story1_template(self):
        response = Client().get('/story1/')
        self.assertTemplateUsed(response, 'story1.html')

    def test_url_profile(self):
        response = Client().get('/profile/')
        self.assertEqual(200, response.status_code)

    def test_profile_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'index.html')

    def test_url_record(self):
        response = Client().get('/trackrecord/')
        self.assertEqual(200, response.status_code)

    def test_record_template(self):
        response = Client().get('/trackrecord/')
        self.assertTemplateUsed(response, 'trackrecord.html')

    def test_url_gallery(self):
        response = Client().get('/gallery/')
        self.assertEqual(200, response.status_code)

    def test_gallery_template(self):
        response = Client().get('/gallery/')
        self.assertTemplateUsed(response, 'gallery.html')

    # Tests for Story 5

    def test_url_courses(self):
        response = Client().get('/courses/')
        self.assertEqual(200, response.status_code)

    def test_courses_template(self):
        response = Client().get('/courses/')
        self.assertTemplateUsed(response, 'courses/my-courses.html')

    def test_url_course_form(self):
        response = Client().get('/courses/add/')
        self.assertEqual(200, response.status_code)

    def test_course_form_template(self):
        response = Client().get('/courses/add/')
        self.assertTemplateUsed(response, 'courses/add-course.html')

    def test_get_course_url(self):
        models.Course.objects.create(
            name="ABC", 
            lecturer="John Doe", 
            term="Odd Term 2020/2021", 
            credits="1 SKS", 
            classroom="1.234", 
            description="Good."
            )
        course = models.Course.objects.get(id=1)
        self.assertEqual(course.get_url(), '/courses/1/')

    def test_course_model(self):
        models.Course.objects.create(
            name="ABC", 
            lecturer="John Doe", 
            term="Odd Term 2020/2021", 
            credits="1 SKS", 
            classroom="1.234", 
            description="Good."
            )
        data = Course.objects.all().count()
        self.assertEqual(data, 1)

    # Tests for Story 6

    def test_url_activities(self):
        response = Client().get('/activities/')
        self.assertEqual(200, response.status_code)

    def test_activities_template(self):
        response = Client().get('/activities/')
        self.assertTemplateUsed(response, 'activities.html')

    def test_activities(self):
        response = Client().get('/activities/')
        fields = response.content.decode('utf8')
        self.assertIn("ACTIVITIES", fields)
        self.assertIn("Add Activity", fields)
        self.assertIn("Join Activity", fields)

    def test_url_activity_form(self):
        response = Client().get('/activities/add-activity/')
        self.assertEqual(200, response.status_code)
    
    def test_activity_form_template(self):
        response = Client().get('/activities/add-activity/')
        self.assertTemplateUsed(response, 'add-activity.html')

    def test_activity_form(self):
        response = Client().get('/activities/add-activity/')
        form_fields = response.content.decode('utf8')
        self.assertIn("Name", form_fields)
        self.assertIn("Submit", form_fields)
    
    def test_url_participant_form(self):
        response = Client().get('/activities/join-activity/')
        self.assertEqual(200, response.status_code)
    
    def test_participant_form_template(self):
        response = Client().get('/activities/join-activity/')
        self.assertTemplateUsed(response, 'activity-form.html')

    def test_participant_form(self):
        response = Client().get('/activities/join-activity/')
        form_fields = response.content.decode('utf8')
        self.assertIn("Name", form_fields)
        self.assertIn("Activity", form_fields)
        self.assertIn("Submit", form_fields)
    
    def test_activity_model(self):
        Activity.objects.create(name="Study Group")
        data = Activity.objects.all().count()
        self.assertEquals(data, 1)

    def test_activity_model_and_participant_model(self):
        Activity.objects.create(name="Study Group")
        their_activity = Activity.objects.get(name="Study Group")
        Participant.objects.create(name="John Doe", activity=their_activity)
        Participant.objects.create(name="Peter Parker", activity=their_activity)
        Participant.objects.create(name="Wally West", activity=their_activity)
        data = Participant.objects.all().count()
        self.assertEquals(data, 3)
    