from django import forms
from .models import Course, Activity, Participant
from django.forms import *

TERMS = [
    ("", "-"),
    ("Odd Term 2020/2021", "Odd Term 2020/2021"),
    ("Even Term 2020/2021", "Even Term 2020/2021"),
    ("Odd Term 2019/2020", "Odd Term 2019/2020"),
    ("Even Term 2019/2020", "Even Term 2019/2020"),
]

CREDITS = [
    ("", "-"),
    ("1 SKS", "1 SKS"),
    ("2 SKS", "2 SKS"),
    ("3 SKS", "3 SKS"),
    ("4 SKS", "4 SKS"),
    ("5 SKS", "5 SKS"),
    ("6 SKS", "6 SKS"),
    ("7 SKS", "7 SKS"),
    ("8 SKS", "8 SKS"),
]

class CourseForm(ModelForm):
    class Meta:
        model = Course
        fields = "__all__"
        widgets = {
            'name': TextInput(attrs={
                'placeholder': 'ex: Web Design',
                'style': 'width:700px; height:35px; padding: 6px 10px;',
                'required': True
            }),
             'lecturer': TextInput(attrs={
                'placeholder': 'ex: John Doe',
                'style': 'width:700px; height:35px; padding: 6px 10px;', 
                'required': True
            }),
            'term': Select(choices=TERMS, attrs={
                'class': 'form-control',
                'style': 'height:35px; padding: 6px 10px;', 
                'required': True
            }),
            'credits': Select(choices=CREDITS, attrs={
                'class': 'form-control',
                'style': 'height:35px; padding: 6px 10px;', 
                'required': True
            }),
            'classroom': TextInput(attrs={
                'placeholder': 'ex: 2.2304',
                'style': 'width:700px; height:35px; padding: 6px 10px;',
                'required': True
            }),
            'description': Textarea(attrs={
                'rows': 5,
                'style': 'width:700px; padding: 6px 10px;', 
                'placeholder': 'This course is about...',
                'resize': 'none', 
                'required': True,
            }),
        }

class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        fields = "__all__"
        widgets = {
            'name': TextInput(attrs={
                'placeholder': 'ex: Study Group', 
                'style': 'width:700px; height:35px; padding: 6px 10px;',
                'required': True}),
        }

class ParticipantForm(ModelForm):
    class Meta:
        model = Participant
        fields = "__all__"
        widgets = {
            'name': TextInput(attrs={
                'placeholder': 'ex: John Doe',
                'style': 'width:700px; height:35px; padding: 6px 10px;', 
                'required': True}),
        }