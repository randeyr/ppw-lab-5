from django.urls import path
from . import views

app_name = 'profilemodel'

urlpatterns = [
    path('', views.home, name='home'),
    path('story1/', views.story1, name='story1'),
    path('profile/', views.index, name='profile'),
    path('trackrecord/', views.trackrecord, name='trackrecord'),
    path('gallery/', views.gallery, name='gallery'),
    path('courses/', views.courses, name='courses'),
    path('courses/<int:course_id>/', views.course_details, name='course_details'),
    path('courses/add/', views.add_course, name='add_course'),
    path('courses/<int:course_id>/delete/', views.delete_course, name='delete_course'),
    path('activities/', views.activities),
    path('activities/add-activity/', views.add_activity),
    path('activities/join-activity/', views.join_activity),
]