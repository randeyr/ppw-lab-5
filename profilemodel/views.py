from django.shortcuts import render, redirect
from .models import Course, Activity, Participant
from .forms import CourseForm, ActivityForm, ParticipantForm
from django.http import HttpResponse
from userbase import views

# functions for story 3

def home(request):
    return render(request, 'homepage.html')

def story1(request):
    return render(request, 'story1.html')

def index(request):
    return render(request, 'index.html')

def trackrecord(request):
    return render(request, 'trackrecord.html')

def gallery(request):
    return render(request, 'gallery.html')

# functions for story 5

def courses(request):
    courses = Course.objects.all()
    response = {
        'courses': courses
    }
    return render(request, 'courses/my-courses.html', response)

def course_details(request, course_id):
    course = Course.objects.get(id=course_id)
    response = {
        'course': course
    }
    return render(request, 'courses/course-details.html', response)


def add_course(request):
    form = CourseForm(request.POST or None)
    if form.is_valid():
        Course.objects.create(**form.cleaned_data)
        return redirect('/courses')
    response = {
        'form': form
    }
    return render(request, "courses/add-course.html", response)

def delete_course(request, course_id):
    course = Course.objects.get(id=course_id)
    if request.method == 'POST':
        course.delete()
        return redirect('/courses')
    response = {
        'course': course
    }
    return render(request, 'courses/delete-course.html', response)

# functions for story 6

def activities(request):
    activities = Activity.objects.all()
    participants = Participant.objects.all()
    response= {
        'activities' : activities,
        'participants': participants
    }
    return render(request, 'activities.html', response)

def add_activity(request):
    form = ActivityForm(request.POST or None)
    if form.is_valid():
        Activity.objects.create(**form.cleaned_data)
        return redirect('/activities')
    response = {
        'form': form
    }
    return render(request, 'add-activity.html', response)

def join_activity(request):
    form = ParticipantForm(request.POST or None)
    if form.is_valid():
        Participant.objects.create(**form.cleaned_data)
        return redirect('/activities')
    response = {
        'form': form
    }
    return render(request, 'activity-form.html', response)