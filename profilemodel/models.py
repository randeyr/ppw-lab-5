from django.db import models
from django.urls import reverse

TERMS = [
    ("Odd Term 2020/2021", "Odd Term 2020/2021"),
    ("Even Term 2020/2021", "Even Term 2020/2021"),
    ("Odd Term 2019/2020", "Odd Term 2019/2020"),
    ("Even Term 2019/2020", "Even Term 2019/2020"),
]

CREDITS = [
    ("1 SKS", "1 SKS"),
    ("2 SKS", "2 SKS"),
    ("3 SKS", "3 SKS"),
    ("4 SKS", "4 SKS"),
    ("5 SKS", "5 SKS"),
    ("6 SKS", "6 SKS"),
    ("7 SKS", "7 SKS"),
    ("8 SKS", "8 SKS"),
]

class Course(models.Model):
    name            = models.CharField(max_length = 50)
    lecturer        = models.CharField(max_length = 50)
    term            = models.CharField(max_length = 50, choices=TERMS)
    credits         = models.CharField(max_length = 10, choices=CREDITS)
    classroom       = models.CharField(max_length = 20)
    description     = models.TextField(max_length = 400)

    def __str__(self):
        return self.name

    def get_url(self):
        return f"/courses/{self.id}/"

class Activity(models.Model):
    name           = models.CharField(max_length = 50) 

    def __str__(self):
        return self.name

class Participant(models.Model):
    name            = models.CharField(max_length = 50)
    activity        = models.ForeignKey(Activity, on_delete=models.CASCADE)

    def __str__(self):
        return self.name