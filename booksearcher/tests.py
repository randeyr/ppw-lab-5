from django.test import TestCase,Client
from . import views
from django.urls import resolve
import requests, json

class BookSearcherTest(TestCase):

    def test_url(self):
        response = Client().get('/searchbook/')
        self.assertEqual(response.status_code,200)

    def test_template(self):
        response = Client().get('/searchbook/')
        self.assertTemplateUsed(response,'booksearcher.html')

    def test_web_page(self):
        response = Client().get('/searchbook/')
        fields = response.content.decode('utf8')
        self.assertIn("FIND A BOOK", fields)
        self.assertIn("Cover", fields)
        self.assertIn("Title", fields)
        self.assertIn("Author", fields)
        self.assertIn("Preview", fields)

    def test_func_book_search(self):
        find = resolve('/searchbook/')
        self.assertEqual(find.func, views.book_search)
    '''
    def test_func_search_results(self):
        input_ex = "eco"
        response = Client().get('/results?q=' + input_ex)
        url = "https://www.googleapis.com/books/v1/volumes?q=" + input_ex
        data = requests.get(url)
        data_from_google_url = json.loads(data.content)
        data_from_search_results_func = json.loads(response.content)
        self.assertEqual(data_from_google_url,data_from_search_results_func)
    '''