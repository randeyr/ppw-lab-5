from django.urls import path
from . import views

app_name = 'booksearcher'

urlpatterns = [
    path('searchbook/', views.book_search, name='booksearch'),
    path('results/', views.search_results, name='results'),
]