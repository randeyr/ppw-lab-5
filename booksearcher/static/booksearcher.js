$("#search").keyup(function(){
    var input = $("#search").val();
    $.ajax({
        url : '/results?q=' + input,
        success : function(book) {
            var books = book.items;
            $("tbody").empty();
            for (i = 0; i < books.length; i++) {
                var html = "<tr><td>";
                var cover = books[i].volumeInfo.imageLinks.smallThumbnail;
                html += "<img src=" + cover + "></td>";
                var title = books[i].volumeInfo.title;
                html += "<td>" + title + "</td>";
                var author = books[i].volumeInfo.authors[0];
                html += "<td>" + author + "</td>";
                var link = books[i].volumeInfo.previewLink;
                html += "<td><a href=" + link + "target='_blank'>link</a></td></tr>";
                $("tbody").append(html);
            }
        }
    });
});