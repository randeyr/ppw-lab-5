from django.apps import AppConfig


class BooksearcherConfig(AppConfig):
    name = 'booksearcher'
