from django.shortcuts import render
from django.http import JsonResponse
import requests, json

def book_search(request):
    return render(request, "booksearcher.html")

def search_results(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    data = requests.get(url)
    results = json.loads(data.content)
    return JsonResponse(results)