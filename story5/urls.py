from django.urls import path, include
from django.contrib import admin

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('profilemodel.urls')),
    path('aboutme/', include('accordion.urls')),
    path('', include('booksearcher.urls')),
    path('', include('userbase.urls')),
    path('', include('django.contrib.auth.urls')),
]